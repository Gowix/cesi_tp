# I. Base de données

> A faire uniquement sur `db.tp2.cesi`.

Installer le paquet `mariadb-server`, puis démarrer le service associé.

Il faudra créer un utilisateur dans l'instance de base de données, ainsi qu'une base dédiée. Référez-vous à des docs/articles que vous pourrez trouver sur internet concernant la préparation d'une base de données pour une utilisation avec Wordpress. Les étapes :
* se connecter à la base
* créer une base
* créer un utilisateur
* attribuer les droits sur la base de données à l'utilisateur 
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base

Pour connaître le port utilisé par la base de données, on peut uiliser la commande `ss`. Une fois la base de données lancée, on peut par exemple la commande suivante pour déterminer le port utilisé par la base :
```bash
$ sudo ss -alnpt
```

Pour tester votre base de données :
```bash
$ mysql -u <USER> -p -h <IP_BASE_DE_DONNEES> -P <PORT_BASE_DE_DONNEES>

# Exemple de commande SQL à taper pour vérifier que la base fonctionne
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
+--------------------+
2 rows in set (0.00 sec)
```

```bash=
Installation, configuration mariadb

yum install mariadb-server -y

Installé :
  mariadb-server.x86_64 1:5.5.68-1.el7

Dépendances installées :
  perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7                perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7
  perl-DBD-MySQL.x86_64 0:4.023-6.el7                         perl-DBI.x86_64 0:1.627-4.el7
  perl-Data-Dumper.x86_64 0:2.145-3.el7                       perl-IO-Compress.noarch 0:2.061-2.el7
  perl-Net-Daemon.noarch 0:0.48-5.el7                         perl-PlRPC.noarch 0:0.2020-14.el7
  
sudo systemctl start mariadb

sudo mysql_secure_installation
[...]
Thanks for using MariaDB!

mysql -u root -p 

CREATE DATABASE wordpress;
Query OK, 1 row affected (0.00 sec)

CREATE USER 'wordpress'@'db' IDENTIFIED BY 'wordpress';
Query OK, 0 rows affected (0.00 sec)
 
GRANT ALL PRIVILEGES ON wordpress.* *** **TO wordpress@db;
Query OK, 0 rows affected (0.00 sec)

sudo firewall-cmd --add-port=3306/tcp --permanent
success

sudo firewall-cmd --reload
success

```

# II. Serveur Web

> A faire uniquement `web.tp2.cesi`

Installer un serveur Apache (paquet `httpd` dans CentOS).

Télécharger Wordpress : https://wordpress.org/latest.tar.gz

Idem ici, référez-vous à un des milliers d'article/doc/tuto que vous pourrez trouver. Faites vos recherches en anglais et précisez l'OS qu'on utilise.

Les étapes :
* installer le serveur Web (`httpd`) et le langage PHP (pour installer PHP, voir les instructions plus bas)
* télécharger wordpress
* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)
* configurer Apache pour qu'il serve le dossier où se trouve Wordpress
* lancer le serveur de base de donnée, puis le serveur web
* ouvrir le port firewall sur le serveur web
* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

Installation de PHP : la version de PHP disponible dans les dépôts officiels de CentOS7 n'est pas compatible avec la dernière version de Wrodpress disponible. Pour installer un version de PHP compatible :
```bash
# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
```

#### Résultat config WEB

```bash=
**yum install httpd -y**
Installé :
  httpd.x86_64 0:2.4.6-97.el7.centos

Dépendances installées :
  apr.x86_64 0:1.4.8-7.el7            apr-util.x86_64 0:1.5.2-6.el7       httpd-tools.x86_64 0:2.4.6-97.el7.centos
  mailcap.noarch 0:2.1.41-2.el7

Terminé !

**sudo yum install wget**
Installation : wget-1.14-18.el7_6.1.x86_64                                                                        1/1
Vérification : wget-1.14-18.el7_6.1.x86_64                                                                        1/1

Installé :
wget.x86_64 0:1.14-18.el7_6.1

**wget https://wordpress.org/latest.tar.gz**
100%[==============================================================================>] 15 422 346  8,81MB/s   ds 1,7s

2021-01-06 11:55:41 (8,81 MB/s) - «latest.tar.gz» sauvegardé [15422346/15422346]

**tar xzvf latest.tar.gz**
wordpress/ [...] wordpress/wp-comments-post.php

**sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y**
[...]
Installé :
  remi-release.noarch 0:7.9-1.el7.remi

Dépendances installées :
  epel-release.noarch 0:7-11

Terminé !

**sudo yum install -y yum-utils**
Installé :
  yum-utils.noarch 0:1.1.31-54.el7_8

Dépendances installées :
  libxml2-python.x86_64 0:2.9.1-6.el7.5    python-chardet.noarch 0:2.2.1-3.el7    python-kitchen.noarch 0:1.1.1-5.el7

Terminé !

**sudo yum remove -y php**
Aucun paquet marqué pour suppression

**sudo yum-config-manager --enable remi-php56**

**sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y**

Installé :
  php.x86_64 0:5.6.40-24.el7.remi                          php-cli.x86_64 0:5.6.40-24.el7.remi
 [...]

Dépendances installées :
  dejavu-fonts-common.noarch 0:2.33-6.el7                     dejavu-sans-fonts.noarch 0:2.33-6.el7
[...]
  php-pecl-jsonc.x86_64 0:1.3.10-2.el7.remi.5.6               t1lib.x86_64 0:5.1.2-14.el7

Terminé !

**sudo rsync -avP ~/wordpress/ /var/www/html/**
wordpress/wp-content/themes/twentytwentyone/assets/sass/04-elements/forms.scss
          7,067 100%   44.81kB/s    0:00:00 (xfr#818, ir-chk=1073/1971)
[...]
wordpress/wp-includes/widgets/class-wp-widget-tag-cloud.php
          6,726 100%   15.07kB/s    0:00:00 (xfr#2170, to-chk=1/2463)

**sudo mkdir /var/www/html/wp-content/uploads**

**sudo chown -R apache:apache /var/www/html/* *

**sudo cp wp-config-sample.php wp-config.php**
```


# III. Reverse Proxy

> A faire uniquement `rp.tp2.cesi`.

Installer un serveur NGINX (paquet `epel-release` puis `nginx` sur CentOS).

Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80. Une configuration du fichier `/etc/nginx/nginx.conf` extrêmement minimale peut ressembler à :
```
events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        location / {
            proxy_pass   http://<IP:PORT>;
        }
    }
}
```

> Il sera nécessaire de remplacer la mention `<IP:PORT>` par l'IP et le port où est joignable le serveur Web.

Une fois cela effectué, configurer le firewall de NGINX afin d'accepter ce trafic. 

Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy. 

> Pour avoir quelque chose de clean et similaire à un monde réel (mais sans DNS) vous pouvez ajouter au fichier `/etc/hosts` (ou équivalent) du **CLIENT** l'adresse IP du reverse proxy qui pointe vers le nom "web.cesi". Ainsi le client accède à l'application sur le nom `http://web.cesi`.

### Résultat Reverse Proxy

```bash=
sudo yum install epel-release -y
sudo yum install nginx -y

sudo vim /etc/nginx/nginx.conf

systemctl enable nginx

systemctl status nginx

● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2021-01-07 13:07:19 CET; 14min ago
  Process: 1917 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1915 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1914 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1919 (nginx)
   CGroup: /system.slice/nginx.service
           ├─1919 nginx: master process /usr/sbin/nginx
           └─1920 nginx: worker process
[...]

sudo cat /etc/nginx/nginx.conf

events {}

http {
    server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://192.168.237.11:80;
        }
    }
}

sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --reload

```

# IV. Un peu de sécu

## 2. HTTPS

### A. Génération du certificat et de la clé privée associée

> A faire sur la machine frontale Web : le reverse proxy.

Commande pour générer une clé et un certificat auto-signé :
```bash
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
```

**NB :** lorsque vous taperez la commande de génération de certificat, un prompt vous demandera des infos qui seront inscrites dans le certificat. Vous pouvez mettre ce que vous voulez **SAUF** pour l'étape où on vous demande :
```bash
Common Name (e.g. server FQDN or YOUR name) []:
```
**Il est impératif de saisir ici le FQDN ou hostname qui sera utilisé par les clients pour joindre le service.** Dans notre cas c'est `web.cesi`.

Le certificat et la clé ont été générés dans le répertoire courant. Vous pouvez les déplacez où vous le souhaitez ; il existe cependant un path réservé à ça dans CentOS :
* `/etc/pki/tls/certs/` pour les certificats
* `/etc/pki/tls/private/` pour les clés

### B. Configurer le reverse proxy

> A faire sur la machine frontale Web : le reverse proxy.

Configurer le reverse proxy pour écouter sur le port 443 (TCP). Le port 443 doit mettre en place un chiffrement TLS avec la clé et le certificat qui viennent d'être générés. Il doit toujours rediriger vers l'application Web Wordpress.

Si le port 80 est atteint, le trafic doit être redirigé automatiquement vers le port 443 (ça se fait dans la configuration de NGINX).

> Gardez en tête la commande `sudo ss -alnpt` pour avoir une idée des ports TCP en écoute sur votre machine. Vous pouvez vous assurer que votre serveur tourne bien sur le port `80/tcp` et `443/tcp` de cette façon. Pour tester la redirection automatique de port 80 vers 443, il faudra vous connecter au site comme un client normal, avec un navigateur.

N'oubliez pas d'ajouter au fichier `hosts` du CLIENT que `web.cesi` doit pointer vers l'adresse IP du reverse proxy. Si vous testez avec votre machine hôte et votre navigateur, c'est au fichier `hosts` de VOTRE PC qu'il faut ajouter l'IP et le nom `web.cesi`.

### Résultat partie A, B

```bash=
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout cesi.key -out cesi.crt

sudo vim /etc/nginx/nginx.conf

cat /etc/nginx/nginx.conf

events {}

http {
    server {
        listen       443;

        ssl_certificate         /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key     /etc/pki/tls/private/cesi.key;

        ssl_session_cache  builtin:1000  shared:SSL:10m;
        ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
        ssl_prefer_server_ciphers on;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://192.168.237.11:80;

        proxy_redirect http://192.168.237.11:80 https://192.168.237.11:443;
        }
    }
}
```
